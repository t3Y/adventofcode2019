#!/usr/bin/env python3

class Computer:

    # load a program and reset all of the computer's parameters
    def load(self, program):
        self.memory = program[:]
        self.pointer = 0
        self.inputs = None
        self.relativeBase = 0

    #give the computer a preset sequence for inputs to feed the program when it's prompting
    def queueInput(self, value):
        if self.inputs == None:
            self.inputs = [value]
        else:
            self.inputs.append(value)

    def input(self):
        if self.inputs != None and len(self.inputs) > 0:
            return self.inputs.pop(0)
        else:
            return int(input("Waiting for input: "))

    # read the value in memory at the given address, if address is out of bounds, expand memory
    def readAt(self, address):
        while address >= len(self.memory):
            self.memory.append(0)
        return self.memory[address]


    # write the value in memory at the given address, if address is out of bounds, expand memory
    def writeAt(self, address, value):
        while address >= len(self.memory):
            self.memory.append(0)
        self.memory[address] = value

    def getParamValue(self, mode, paramID):
        if mode == 0:
            address = self.readAt(self.pointer + paramID)
        elif mode == 1:
            address = self.pointer + paramID
        elif mode == 2:
            address = self.readAt(self.pointer + paramID) + self.relativeBase

        return self.readAt(address)

    def getParamAsAddress(self, mode, paramID):
        if mode == 0:
            address = self.readAt(self.pointer + paramID)
        elif mode == 1:
            address = self.pointer + paramID
        elif mode == 2:
            address = self.readAt(self.pointer + paramID) + self.relativeBase

        return address

    #run the program, if pauseOnOutput, return output at first output instruction
    def run(self, pauseOnOutput):

        done = False

        while not done:
            
            instruction = self.memory[self.pointer]

            opcode = instruction % 100
            parameterModes = instruction // 100

            if opcode == 1:
                # add instructions, takes three parameters
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10
                param3Mode = (parameterModes // 100) % 10

                value1 = self.getParamValue(param1Mode, 1)
                value2 = self.getParamValue(param2Mode, 2)

                address = self.getParamAsAddress(param3Mode, 3)


                self.writeAt(address, value1 + value2)

                # move self.pointer 4 steps forward
                self.pointer += 4

            elif opcode == 2:
                # mult instructions, takes three parameters
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10
                param3Mode = (parameterModes // 100) % 10

                value1 = self.getParamValue(param1Mode, 1)
                value2 = self.getParamValue(param2Mode, 2)

                address = self.getParamAsAddress(param3Mode, 3)


                self.writeAt(address, value1 * value2)

                # move self.pointer 4 steps forward
                self.pointer += 4

            elif opcode == 3:
                # read instruction, takes one parameter
                param1Mode = parameterModes % 10

                address = self.getParamAsAddress(param1Mode, 1)
                value = self.input()

                self.writeAt(address, value)

                # move self.pointer two steps further
                self.pointer += 2

            elif opcode == 4:
                # output instruction, takes one parameter
                param1Mode = parameterModes % 10

                value = self.getParamValue(param1Mode, 1)

                # move self.pointer two steps further
                self.pointer +=2

                if pauseOnOutput:
                    return value
                else:
                    print(value)

            elif opcode == 5:
                # jumpiftrue instruction, takes two parameters
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                value1 = self.getParamValue(param1Mode, 1)
                address = self.getParamValue(param2Mode, 2)


                #adjust self.pointer
                if value1 != 0:
                    self.pointer = address
                else: 
                    self.pointer += 3

            elif opcode == 6:
                # jumpiffalse instruction, takes two parameters
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                value1 = self.getParamValue(param1Mode, 1)
                address = self.getParamValue(param2Mode, 2)


                #adjust self.pointer
                if value1 == 0:
                    self.pointer = address
                else: 
                    self.pointer += 3
            
            elif opcode == 7:
                # lessthan instruction, takes three parameters
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10
                param3Mode = (parameterModes // 100) % 10

                value1 = self.getParamValue(param1Mode, 1)
                value2 = self.getParamValue(param2Mode, 2)

                address = self.getParamAsAddress(param3Mode, 3)

                value = 0
                if value1 < value2:
                    value = 1

                self.writeAt(address, value)

                # adjust self.pointer
                self.pointer += 4

            elif opcode == 8:
                # equals instruction, takes three parameters
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10
                param3Mode = (parameterModes // 100) % 10

                value1 = self.getParamValue(param1Mode, 1)
                value2 = self.getParamValue(param2Mode, 2)

                address = self.getParamAsAddress(param3Mode, 3)

                value = 0
                if value1 == value2:
                    value = 1

                self.writeAt(address, value)

                # adjust self.pointer
                self.pointer += 4

            elif opcode == 9:
                # adjust relative base instruction, takes one parameter, can be in either base
                param1Mode = parameterModes % 10

                value = self.getParamValue(param1Mode, 1)

                self.relativeBase += value

                self.pointer += 2
            
            elif opcode == 99:
                # end instruction, we're done
                return "DONE"

            else:
                # something went wrong
                done = True
                print("Something went wrong: invalid instruction {} at {}".format(instruction, self.pointer))

