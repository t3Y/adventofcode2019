#!/usr/bin/env python3


class Reaction(object):

    def __init__(self, elements, quantities, result, result_quantity):
        self.elements = elements
        self.quantities = quantities
        self.result = result
        self.result_quantity = result_quantity
