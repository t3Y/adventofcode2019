#!/usr/bin/env python3
from math import ceil
from copy import deepcopy
from reaction import Reaction

def main():

    input_file = open("./input", "r")
    lines = [l.rstrip() for l in input_file]
    input_file.close()

    reactions = {}
    excess = {}

    for line in lines:
        temp = line.split(" => ")
        inputs = temp[0].split(", ")
        result = temp[1].split(" ")[1]
        result_quantity = int(temp[1].split(" ")[0])

        quantities = []
        elements = []
        for inp in inputs:
            quantities.append(int(inp.split(" ")[0]))
            elements.append(inp.split(" ")[1])

        reaction = Reaction(elements, quantities, result, result_quantity)
        reactions[result] = reaction
        excess[result] = 0

    quantity = 1

    step = 1
    target = 1000000000000
    
    while step > 0: 
        result = determine_ore_requirements(deepcopy(excess), reactions, quantity + step)
        if target - result >= 0:
            quantity += step
            step *= 2

        else:
            step = step // 2

    print(quantity)

def determine_ore_requirements(excess, reactions, quantity):

    return get_requirements(excess, reactions, reactions["FUEL"], quantity)

def get_requirements(excess, reactions, reaction, quantity):
    if excess[reaction.result] > 0:
        headstart = min(quantity, excess[reaction.result])
        excess[reaction.result] -= headstart
        quantity -= headstart

    if quantity == 0:
        return 0

    min_repeats = max(int(ceil(quantity / reaction.result_quantity)), 1)

    excess[reaction.result] += reaction.result_quantity * min_repeats - quantity

    if reaction.elements[0] == "ORE":

        return reaction.quantities[0] * min_repeats

    total_requirements = 0

    for index in range(len(reaction.elements)):
        total_requirements += get_requirements(excess, reactions, reactions[reaction.elements[index]], reaction.quantities[index] * min_repeats)

    return total_requirements

main()
