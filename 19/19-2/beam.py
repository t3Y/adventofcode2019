#!/usr/bin/env python3

from computer import Computer

def main():
    """
    Part 2, for each candidate in the beam, check if it's the upper left corner of a 100x100 square within the beam
    skip 100 columns if last checked column contains no valid candidate backtrack when we find one valid candidate
    binary search would be a lot faster but hey this works
    """

    program = [109, 424, 203, 1, 21102, 11, 1, 0, 1105, 1, 282, 21101, 0, 18, 0, 1105, 1, 259, 2101, 0, 1, 221, 203, 1, 21102, 1, 31, 0, 1105, 1, 282, 21102, 1, 38, 0, 1105, 1, 259, 20102, 1, 23, 2, 21201, 1, 0, 3, 21102, 1, 1, 1, 21102, 57, 1, 0, 1106, 0, 303, 2102, 1, 1, 222, 21002, 221, 1, 3, 20101, 0, 221, 2, 21101, 0, 259, 1, 21101, 0, 80, 0, 1105, 1, 225, 21101, 44, 0, 2, 21102, 91, 1, 0, 1105, 1, 303, 1202, 1, 1, 223, 21002, 222, 1, 4, 21102, 259, 1, 3, 21102, 1, 225, 2, 21102, 225, 1, 1, 21101, 118, 0, 0, 1106, 0, 225, 21002, 222, 1, 3, 21101, 163, 0, 2, 21101, 0, 133, 0, 1106, 0, 303, 21202, 1, -1, 1, 22001, 223, 1, 1, 21102, 148, 1, 0, 1106, 0, 259, 1202, 1, 1, 223, 20101, 0, 221, 4, 21001, 222, 0, 3, 21102, 1, 24, 2, 1001, 132, -2, 224, 1002, 224, 2, 224, 1001, 224, 3, 224, 1002, 132, -1, 132, 1, 224, 132, 224, 21001, 224, 1, 1, 21101, 195, 0, 0, 105, 1, 108, 20207, 1, 223, 2, 21002, 23, 1, 1, 21102, -1, 1, 3, 21102, 1, 214, 0, 1106, 0, 303, 22101, 1, 1, 1, 204, 1, 99, 0, 0, 0, 0, 109, 5, 2101, 0, -4, 249, 22102, 1, -3, 1, 22101, 0, -2, 2, 22101, 0, -1, 3, 21102, 250, 1, 0, 1106, 0, 225, 21202, 1, 1, -4, 109, -5, 2105, 1, 0, 109, 3, 22107, 0, -2, -1, 21202, -1, 2, -1, 21201, -1, -1, -1, 22202, -1, -2, -2, 109, -3, 2106, 0, 0, 109, 3, 21207, -2, 0, -1, 1206, -1, 294, 104, 0, 99, 22102, 1, -2, -2, 109, -3, 2105, 1, 0, 109, 5, 22207, -3, -4, -1, 1206, -1, 346, 22201, -4, -3, -4, 21202, -3, -1, -1, 22201, -4, -1, 2, 21202, 2, -1, -1, 22201, -4, -1, 1, 21202, -2, 1, 3, 21101, 0, 343, 0, 1106, 0, 303, 1106, 0, 415, 22207, -2, -3, -1, 1206, -1, 387, 22201, -3, -2, -3, 21202, -2, -1, -1, 22201, -3, -1, 3, 21202, 3, -1, -1, 22201, -3, -1, 2, 21201, -4, 0, 1, 21101, 384, 0, 0, 1105, 1, 303, 1105, 1, 415, 21202, -4, -1, -4, 22201, -4, -3, -4, 22202, -3, -2, -2, 22202, -2, -4, -4, 22202, -3, -2, -3, 21202, -4, -1, -2, 22201, -3, -2, 1, 21202, 1, 1, -4, 109, -5, 2105, 1, 0]

    comp = Computer()
    crossed_beam = False
    first_positive = True
    width = 100
    lower_bound = 0
    i = 0
    while i < 10000:
        for j in range(lower_bound, 10000):
            state = check_coords(comp, program, i, j)
            if state == 1 and not crossed_beam:
                crossed_beam = True
                lower_bound = j
            if state == 0:
                if crossed_beam:
                    crossed_beam = False
                    break
                else:
                    continue
            if is_square(comp, program, i, j, width-1):
                if first_positive:
                    first_positive = False
                    i = max(0, i - width)
                else:
                    print(i * 10000 + j)
                    return
        if first_positive:
            i += width
        else:
            i += 1

def is_square(comp, program, x_pos, y_pos, width):
    output = (check_coords(comp, program, x_pos, y_pos)
              and check_coords(comp, program, x_pos + width, y_pos)
              and check_coords(comp, program, x_pos + width, y_pos + width)
              and check_coords(comp, program, x_pos, y_pos + width))
    return output

def check_coords(comp, program, x_pos, y_pos):
    comp.load(program)
    comp.queue_input(x_pos)
    comp.queue_input(y_pos)
    return comp.run(True) == 1

main()
