#!/usr/bin/env python3
"""
module containing the Computer object
"""

class Computer(object):
    """
    An intcode computer, runs intcode programs and provides
    all necessary methods to queue input, pause on outputs etc..
    """

    def __init__(self):
        self.memory = []
        self.pointer = 0
        self.inputs = None
        self.relative_base = 0

    # load a program and reset all of the computer's parameters
    def load(self, program):
        self.memory = program[:]
        self.pointer = 0
        self.inputs = None
        self.relative_base = 0

    #give the computer a preset sequence for inputs to feed the program when it's prompting
    def queue_input(self, value):
        if self.inputs is None:
            self.inputs = [value]
        else:
            self.inputs.append(value)

    def input(self):
        if self.inputs != None and not self.inputs:
            return self.inputs.pop(0)
        return int(input("Waiting for input: "))

    # read the value in memory at the given address, if address is out of bounds, expand memory
    def read_at(self, address):
        while address >= len(self.memory):
            self.memory.append(0)
        return self.memory[address]


    # write the value in memory at the given address, if address is out of bounds, expand memory
    def write_at(self, address, value):
        while address >= len(self.memory):
            self.memory.append(0)
        self.memory[address] = value

    def get_param_value(self, mode, param_id):
        if mode == 0:
            address = self.read_at(self.pointer + param_id)
        elif mode == 1:
            address = self.pointer + param_id
        elif mode == 2:
            address = self.read_at(self.pointer + param_id) + self.relative_base

        return self.read_at(address)

    def get_param_as_address(self, mode, param_id):
        if mode == 0:
            address = self.read_at(self.pointer + param_id)
        elif mode == 1:
            address = self.pointer + param_id
        elif mode == 2:
            address = self.read_at(self.pointer + param_id) + self.relative_base

        return address

    def run(self, pause_on_output):
        """
        run the program, if pause_on_output, return output at first output instruction
        """
        done = False

        while not done:

            instruction = self.memory[self.pointer]

            opcode = instruction % 100
            parameter_modes = instruction // 100

            if opcode == 1:
                # add instructions, takes three parameters
                param_1_mode = parameter_modes % 10
                param_2_mode = (parameter_modes // 10) % 10
                param_3_mode = (parameter_modes // 100) % 10

                value1 = self.get_param_value(param_1_mode, 1)
                value2 = self.get_param_value(param_2_mode, 2)

                address = self.get_param_as_address(param_3_mode, 3)


                self.write_at(address, value1 + value2)

                # move self.pointer 4 steps forward
                self.pointer += 4

            elif opcode == 2:
                # mult instructions, takes three parameters
                param_1_mode = parameter_modes % 10
                param_2_mode = (parameter_modes // 10) % 10
                param_3_mode = (parameter_modes // 100) % 10

                value1 = self.get_param_value(param_1_mode, 1)
                value2 = self.get_param_value(param_2_mode, 2)

                address = self.get_param_as_address(param_3_mode, 3)


                self.write_at(address, value1 * value2)

                # move self.pointer 4 steps forward
                self.pointer += 4

            elif opcode == 3:
                # read instruction, takes one parameter
                param_1_mode = parameter_modes % 10

                address = self.get_param_as_address(param_1_mode, 1)
                value = self.input()

                self.write_at(address, value)

                # move self.pointer two steps further
                self.pointer += 2

            elif opcode == 4:
                # output instruction, takes one parameter
                param_1_mode = parameter_modes % 10

                value = self.get_param_value(param_1_mode, 1)

                # move self.pointer two steps further
                self.pointer += 2

                if pause_on_output:
                    return value
                else:
                    print(value)

            elif opcode == 5:
                # jumpiftrue instruction, takes two parameters
                param_1_mode = parameter_modes % 10
                param_2_mode = (parameter_modes // 10) % 10

                value1 = self.get_param_value(param_1_mode, 1)
                address = self.get_param_value(param_2_mode, 2)


                #adjust self.pointer
                if value1 != 0:
                    self.pointer = address
                else:
                    self.pointer += 3

            elif opcode == 6:
                # jumpiffalse instruction, takes two parameters
                param_1_mode = parameter_modes % 10
                param_2_mode = (parameter_modes // 10) % 10

                value1 = self.get_param_value(param_1_mode, 1)
                address = self.get_param_value(param_2_mode, 2)


                #adjust self.pointer
                if value1 == 0:
                    self.pointer = address
                else:
                    self.pointer += 3

            elif opcode == 7:
                # lessthan instruction, takes three parameters
                param_1_mode = parameter_modes % 10
                param_2_mode = (parameter_modes // 10) % 10
                param_3_mode = (parameter_modes // 100) % 10

                value1 = self.get_param_value(param_1_mode, 1)
                value2 = self.get_param_value(param_2_mode, 2)

                address = self.get_param_as_address(param_3_mode, 3)

                value = 0
                if value1 < value2:
                    value = 1

                self.write_at(address, value)

                # adjust self.pointer
                self.pointer += 4

            elif opcode == 8:
                # equals instruction, takes three parameters
                param_1_mode = parameter_modes % 10
                param_2_mode = (parameter_modes // 10) % 10
                param_3_mode = (parameter_modes // 100) % 10

                value1 = self.get_param_value(param_1_mode, 1)
                value2 = self.get_param_value(param_2_mode, 2)

                address = self.get_param_as_address(param_3_mode, 3)

                value = 0
                if value1 == value2:
                    value = 1

                self.write_at(address, value)

                # adjust self.pointer
                self.pointer += 4

            elif opcode == 9:
                # adjust relative base instruction, takes one parameter, can be in either base
                param_1_mode = parameter_modes % 10

                value = self.get_param_value(param_1_mode, 1)

                self.relative_base += value

                self.pointer += 2

            elif opcode == 99:
                # end instruction, we're done
                return "DONE"

            else:
                # something went wrong
                done = True
                print("Something went wrong: invalid instruction {} at {}".format(instruction, self.pointer))
