#!/usr/bin/env python3

class Computer:

    def load(self, program):
        self.memory = program[:]

    def run(self):
        pointer = 0
        done = False

        while not done:

            instruction = self.memory[pointer]

            opcode = instruction % 100
            parameterModes = instruction // 100

            if opcode == 1:
                # add instructions, parameter modes can vary for param 1 and 2 but is always 0 for param 3
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[pointer+1]

                if param2Mode == 0:
                    address = self.memory[pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[pointer+2]

                param3 = self.memory[pointer+3]

                self.memory[param3] = param1 + param2

                # move pointer 4 steps forward
                pointer += 4

            elif opcode == 2:
                # mult instructions, parameter modes can vary for param 1 and 2 but is always 0 for param 3
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[pointer+1]

                if param2Mode == 0:
                    address = self.memory[pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[pointer+2]

                param3 = self.memory[pointer+3]

                self.memory[param3] = param1 * param2

                # move pointer 4 steps forward
                pointer += 4


            elif opcode == 3:
                # read instruction, parameter mode is always 0
                address = self.memory[pointer+1]
                value = int(input("Waiting for input: "))
                self.memory[address] = value
                # move pointer two steps further
                pointer += 2

            elif opcode == 4:
                # output instruction, parameter mode can be 0 or 1
                if parameterModes == 1:
                    value = self.memory[pointer+1]
                elif parameterModes == 0:
                    address = self.memory[pointer+1]
                    value = self.memory[address]
                print(value)
                # move pointer two steps further
                pointer +=2

            elif opcode == 5:
                # jumpiftrue instruction, takes two parameters, both can be in either mode
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[pointer+1]

                if param2Mode == 0:
                    address = self.memory[pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[pointer+2]

                #adjust pointer
                if param1 != 0:
                    pointer = param2
                else: 
                    pointer += 3

            elif opcode == 6:
                # jumpiffalse instruction, takes two parameters, both can be in either mode
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[pointer+1]

                if param2Mode == 0:
                    address = self.memory[pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[pointer+2]

                #adjust pointer
                if param1 == 0:
                    pointer = param2
                else:
                    pointer += 3
            
            elif opcode == 7:
                # lessthan instruction, takes three parameters, param 3 is always in mode 0
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[pointer+1]

                if param2Mode == 0:
                    address = self.memory[pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[pointer+2]

                param3 = self.memory[pointer+3]
                value = 0

                if param1 < param2:
                    value = 1

                self.memory[param3] = value

                # adjust pointer
                pointer += 4

            elif opcode == 8:
                # equals instruction, takes three parameters, param 3 is always in mode 0
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[pointer+1]

                if param2Mode == 0:
                    address = self.memory[pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[pointer+2]

                param3 = self.memory[pointer+3]
                value = 0

                if param1 == param2:
                    value = 1

                self.memory[param3] = value

                # adjust pointer
                pointer += 4
            
            elif opcode == 99:
                # end instruction, we're done
                done = True

            else:
                # something went wrong
                done = True
                print("Something went wrong: invalid instruction {} at {}".format(instruction, pointer))

