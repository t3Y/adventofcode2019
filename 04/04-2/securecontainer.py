#!/usr/bin/env python3

def main():
    #input
    lowerBound = 146810
    upperBound = 612564

    validPasswords = 0

    for pw in range(lowerBound, upperBound):
        
        if isSorted(pw) and doubledDigit(pw):
            validPasswords += 1

    print(validPasswords)


#check if a digit occurs twice in a row, but no more than twice
def doubledDigit(pw):
    previous = -1

    i = 0
    pwString = str(pw)
    while i < len(pwString):
        if previous == pwString[i]:
            if i == len(pwString)-1 or pwString[i+1] != previous:
                return True
            else:
                while i < len(pwString) and pwString[i] == previous:
                    i += 1
        else:
            previous = pwString[i]
            i += 1

    return False

#check if all digits appear in increasing order
def isSorted(pw):

    maxDigit = 0

    for c in str(pw):
        if int(c) >= maxDigit:
            maxDigit = int(c)
        else:
            return False

    return True

main()
