#!/usr/bin/env python3

def main():
    #input
    lowerBound = 146810
    upperBound = 612564

    validPasswords = 0

    for pw in range(lowerBound, upperBound):
        
        if isSorted(pw) and doubledDigit(pw):
            validPasswords += 1

    print(validPasswords)


#check if a digit appears twice in a row
def doubledDigit(pw):
    previous = -1

    for c in str(pw):
        if previous == c:
            return True
        else:
            previous = c

    return False

#check if all digits appear in increasing order
def isSorted(pw):

    maxDigit = 0

    for c in str(pw):
        if int(c) >= maxDigit:
            maxDigit = int(c)
        else:
            return False

    return True

main()
