#!/usr/bin/env python3

file = open("./input", "r")
modules = file.readlines()
file.close()

totalfuel = 0

for module in modules:
    totalfuel += (int(module) // 3) - 2

print(totalfuel)
