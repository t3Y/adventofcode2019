#!/usr/bin/env python3
def main():
    file = open("./input", "r")
    modules = file.readlines()
    file.close()

    totalfuel = 0

    for module in modules:
        totalfuel += calculate_fuel(int(module))

    print(totalfuel)

def calculate_fuel(mass):
    fuel = (mass // 3) - 2
    if fuel <= 0:
        return 0
    return calculate_fuel(fuel) + fuel

main()
