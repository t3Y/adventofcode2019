# Advent of Code 2019

Advent of Code is a yearly series of daily programming challenges during the first 25 days of December organized by [Eric Wastl](https://was.tl/). You can participate at [adventofcode.com](https://adventofcode.com/).

This repository contains my solutions to the challenges I could solve. For every day there is one folder containing two subfolders, one for each part of the challenge. All solutions are written in python as my intent in participating was to learn the language.

## License

All code is avialable under the MIT Licence. For more information, please refer to the LICENSE file.
