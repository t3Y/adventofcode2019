#!/usr/bin/env python3

import sys

def main():
    file = open("./input", "r")
    fileLines = [list(l.rstrip()) for l in file]
    file.close()

    # adjust indexes to be the same as used the task description
    lines = []
    for i in range(len(fileLines[0])):
        line = []
        for fileLine in fileLines:
            line.append(fileLine[i])
        lines.append(line)


    bestLocationScore = 0
    stationX = 0
    stationY = 0

    for i in range(len(lines)):
        for j in range(len(lines[i])):
            
            if lines[i][j] == "#":
                # new candidate, check lineofSight towards all other asteroids
                score = 0
                
                for x in range(len(lines)):
                    for y in range(len(lines[x])):
                        if (i != x or j != y) and lines[x][y] == "#":
                            a,b = checkLineofSight(i, j, x, y, lines)
                            if a == x and b == y:
                                score += 1


                if score > bestLocationScore:
                    bestLocationScore = score
                    stationX = i
                    stationY = j

    # print(bestLocationScore)
    lazer(stationX, stationY, lines)

# this sort of works, apparently
def lazer(stationX, stationY, lines):

    slopes = []
    classes = {}
    for i in range(len(lines)):
        for j in range(len(lines[i])):

            if lines[i][j] == "#":
            
                key = "E"
                if i < stationX:
                    key = "W"
                div = stationX - i
                if div == 0:
                    if j < stationY:
                        key = "N"
                    elif j > stationY:
                        key = "S"
                else:
                    slope = round((stationY - j) / div, 8)
                    key += str(slope)
                
                if key in classes:
                    classes[key].append((i,j))
                else:
                    if slope not in slopes:
                        slopes.append(slope)
                    classes[key] = [(i,j)]

    slopes.sort()
    destroyed = 0
    cycle = 0
    while len(classes) != 0:
        cycle += 1 
        if "N" in classes:
            north = classes["N"]
            coord = north.pop(getClosestIndex(stationX, stationY, north))
            lines[coord[0]][coord[1]] = "."
            destroyed += 1
            if destroyed == 199:
                print(coord[0]*100 + coord[1])
                return
            if len(north) == 0:
                classes.pop("N")

        for s in reversed(slopes):
            key  = "E" + str(s)
            if key in classes:
                cl = classes[key]
                coord = cl.pop(getClosestIndex(stationX, stationY, cl))
                lines[coord[0]][coord[1]] = "."
                destroyed += 1
                if destroyed == 199:
                    print(coord[0]*100 + coord[1])
                    return
                if len(cl) == 0:
                    classes.pop(key)

        if "S" in classes:
            south = classes["S"]
            coord = south.pop(getClosestIndex(stationX, stationY, south))
            lines[coord[0]][coord[1]] = "."
            destroyed += 1
            if destroyed == 199:
                print(coord[0]*100 + coord[1])
                return
            if len(south) == 0:
                classes.pop("S")

        for s in slopes:
            key  = "W" + str(s)
            if key in classes:
                cl = classes[key]
                coord = cl.pop(getClosestIndex(stationX, stationY, cl))
                lines[coord[0]][coord[1]] = "."
                destroyed += 1
                if destroyed == 199:
                    print(coord[0]*100 + coord[1])
                    return
                if len(cl) == 0:
                    classes.pop(key)
                    

def getClosestIndex(stationX, stationY, coords):
    index = 0
    closestDist = sys.maxsize

    for i in range(len(coords)):
        dist = abs(stationX - coords[i][0]) + abs(stationY - coords[i][1])
        if dist < closestDist:
            closestDist = dist
            index = i

    return index

def checkLineofSight(aX, aY, bX, bY, lines):

    div = (aX - bX)
    # if it's a vertical line, check all coordinates with y value between both
    if div == 0:
        for y in range(min(aY,bY), max(aY,bY)):
            if y == min(aY,bY):
                continue
            if lines[aX][y] == "#":
                return aX,y
        return bX,bY

    # otherwise, determine line equation, calculate Y for all X between both coordinates
    # and check if the result is an asteroid
    slope = (aY - bY) / div
    offset = aY - aX * slope

    for x in range(min(aX,bX), max(aX,bX)):
        if x == min(aX,bX):
            continue
        y = x * slope + offset

        if abs(round(y, 0) - y) < 0.00000001:
            y = int(round(y, 0))
            if lines[x][y] == "#":
                return x,y
    return bX,bY

main()
