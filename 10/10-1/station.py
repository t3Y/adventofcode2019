#!/usr/bin/env python3

def main():
    file = open("./input", "r")
    lines = [l.rstrip() for l in file]
    file.close()

    bestLocationScore = 0

    for i in range(len(lines)):
        for j in range(len(lines[i])):
            
            if lines[i][j] == "#":
                # new candidate, check lineofSight towards all other asteroids
                score = 0
                
                for x in range(len(lines)):
                    for y in range(len(lines[x])):
                        if (i != x or j != y) and lines[x][y] == "#":
                            # coordinates are swapped because we read the input as list of lines
                            score += checkLineofSight(j, i, y, x, lines)

                if score > bestLocationScore:
                    bestLocationScore = score

    print(bestLocationScore)

def checkLineofSight(aX, aY, bX, bY, lines):

    div = (aX - bX)
    # if it's a vertical line, check all coordinates with y value between both
    if div == 0:
        for y in range(min(aY,bY), max(aY,bY)):
            if y == min(aY,bY):
                continue
            if lines[y][aX] == "#":
                return 0
        return 1

    # otherwise, determine line equation, calculate Y for all X between both coordinates
    # and check if the result is an asteroid
    slope = (aY - bY) / div
    offset = aY - aX * slope

    for x in range(min(aX,bX), max(aX,bX)):
        if x == min(aX,bX):
            continue
        y = x * slope + offset

        if abs(round(y, 0) - y) < 0.00000001:
            y = int(round(y, 0))
            if lines[y][x] == "#":
                return 0
    return 1

main()
