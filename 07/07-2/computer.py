#!/usr/bin/env python3

class Computer:

    def load(self, program):
        self.memory = program[:]
        self.pointer = 0
        self.inputs = None

    #give the computer a preset sequence for inputs to feed the program when it's prompting
    def queueInput(self, value):
        if self.inputs == None:
            self.inputs = [value]
        else:
            self.inputs.append(value)

    def input(self):
        if self.inputs != None and len(self.inputs) > 0:
            return self.inputs.pop(0)
        else:
            return int(input("Waiting for input: "))

    #run the program, if pauseOnOutput, return output at first output instruction
    def run(self, pauseOnOutput):

        done = False

        while not done:

            instruction = self.memory[self.pointer]

            opcode = instruction % 100
            parameterModes = instruction // 100

            if opcode == 1:
                # add instructions, parameter modes can vary for param 1 and 2 but is always 0 for param 3
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[self.pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[self.pointer+1]

                if param2Mode == 0:
                    address = self.memory[self.pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[self.pointer+2]

                param3 = self.memory[self.pointer+3]

                self.memory[param3] = param1 + param2

                # move self.pointer 4 steps forward
                self.pointer += 4

            elif opcode == 2:
                # mult instructions, parameter modes can vary for param 1 and 2 but is always 0 for param 3
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[self.pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[self.pointer+1]

                if param2Mode == 0:
                    address = self.memory[self.pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[self.pointer+2]

                param3 = self.memory[self.pointer+3]

                self.memory[param3] = param1 * param2

                # move self.pointer 4 steps forward
                self.pointer += 4


            elif opcode == 3:
                # read instruction, parameter mode is always 0
                address = self.memory[self.pointer+1]
                value = self.input()
                self.memory[address] = value
                # move self.pointer two steps further
                self.pointer += 2

            elif opcode == 4:
                # output instruction, parameter mode can be 0 or 1
                if parameterModes == 1:
                    value = self.memory[self.pointer+1]
                elif parameterModes == 0:
                    address = self.memory[self.pointer+1]
                    value = self.memory[address]

                # move self.pointer two steps further
                self.pointer +=2

                if pauseOnOutput:
                    return value
                else:
                    print(value)

            elif opcode == 5:
                # jumpiftrue instruction, takes two parameters, both can be in either mode
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[self.pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[self.pointer+1]

                if param2Mode == 0:
                    address = self.memory[self.pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[self.pointer+2]

                #adjust self.pointer
                if param1 != 0:
                    self.pointer = param2
                else: 
                    self.pointer += 3

            elif opcode == 6:
                # jumpiffalse instruction, takes two parameters, both can be in either mode
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[self.pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[self.pointer+1]

                if param2Mode == 0:
                    address = self.memory[self.pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[self.pointer+2]

                #adjust self.pointer
                if param1 == 0:
                    self.pointer = param2
                else:
                    self.pointer += 3
            
            elif opcode == 7:
                # lessthan instruction, takes three parameters, param 3 is always in mode 0
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[self.pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[self.pointer+1]

                if param2Mode == 0:
                    address = self.memory[self.pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[self.pointer+2]

                param3 = self.memory[self.pointer+3]
                value = 0

                if param1 < param2:
                    value = 1

                self.memory[param3] = value

                # adjust self.pointer
                self.pointer += 4

            elif opcode == 8:
                # equals instruction, takes three parameters, param 3 is always in mode 0
                param1Mode = parameterModes % 10
                param2Mode = (parameterModes // 10) % 10

                if param1Mode == 0:
                    address = self.memory[self.pointer+1]
                    param1 = self.memory[address]
                elif param1Mode == 1:
                    param1 = self.memory[self.pointer+1]

                if param2Mode == 0:
                    address = self.memory[self.pointer+2]
                    param2 = self.memory[address]
                elif param2Mode == 1:
                    param2 = self.memory[self.pointer+2]

                param3 = self.memory[self.pointer+3]
                value = 0

                if param1 == param2:
                    value = 1

                self.memory[param3] = value

                # adjust self.pointer
                self.pointer += 4
            
            elif opcode == 99:
                # end instruction, we're done
                return "DONE"

            else:
                # something went wrong
                done = True
                print("Something went wrong: invalid instruction {} at {}".format(instruction, self.pointer))

