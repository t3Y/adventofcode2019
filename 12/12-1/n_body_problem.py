#!/usr/bin/env python3
"""
calculate the total energy in the moon system after 100 steps
"""
from moon import Moon
from moon import Vector

def main():

    puzzle_input = "5,-1,5;0,-14,2;16,4,0;18,1,16"
    moons = []

    for line in puzzle_input.split(";"):
        coords = line.split(",")
        moons.append(Moon(int(coords[0]), int(coords[1]), int(coords[2])))

    steps = 0
    while steps < 1000:
        simulate_step(moons)
        steps += 1

    total_energy = 0
    for moon in moons:
        total_energy += moon.total_energy

    print(total_energy)

def simulate_step(moons):
    """
    Simulate a single step of the system
    Every pair of moons is examined and their velocity and position updated
    """

    for i in range(len(moons)):
        for j in range(i, len(moons)):
            moon1 = moons[i]
            moon2 = moons[j]

            moon1_vel_change = Vector(0, 0, 0)
            moon2_vel_change = Vector(0, 0, 0)

            x_diff = moon1.position.x - moon2.position.x
            y_diff = moon1.position.y - moon2.position.y
            z_diff = moon1.position.z - moon2.position.z

            if x_diff < 0:
                moon1_vel_change.x = 1
                moon2_vel_change.x = -1
            elif x_diff > 0:
                moon1_vel_change.x = -1
                moon2_vel_change.x = 1

            if y_diff < 0:
                moon1_vel_change.y = 1
                moon2_vel_change.y = -1
            elif y_diff > 0:
                moon1_vel_change.y = -1
                moon2_vel_change.y = 1

            if z_diff < 0:
                moon1_vel_change.z = 1
                moon2_vel_change.z = -1
            elif z_diff > 0:
                moon1_vel_change.z = -1
                moon2_vel_change.z = 1

            moon1.update_velocity(moon1_vel_change)
            moon2.update_velocity(moon2_vel_change)

    for moon in moons:
        moon.apply_velocity()
        moon.update_energy()

main()
