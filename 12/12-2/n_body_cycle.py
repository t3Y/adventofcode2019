#!/usr/bin/env python3
"""
calculate the number of steps it takes for a system state to repeat
"""
import copy
from moon import Moon
from moon import Vector

def main():

    puzzle_input = "5,-1,5;0,-14,2;16,4,0;18,1,16"
    moons = []

    for line in puzzle_input.split(";"):
        coords = line.split(",")
        moons.append(Moon(int(coords[0]), int(coords[1]), int(coords[2])))


    # the system's state in one dimension is independent from its moons' velocity/position in other dimensions
    # determine the length of a cycle in each dimension, there's a good chance it's shorter than total cycle
    steps = 0
    cycle_x = -1
    cycle_y = -1
    cycle_z = -1

    initial_state = copy.deepcopy(moons)

    while cycle_x == -1 or cycle_y == -1 or cycle_z == -1:
        simulate_step(moons)
        steps += 1

        is_x_cycle = True
        is_y_cycle = True
        is_z_cycle = True
        for i in range(len(moons)):

            if moons[i].position.x != initial_state[i].position.x or moons[i].velocity.x != initial_state[i].velocity.x:
                is_x_cycle = False

            if moons[i].position.y != initial_state[i].position.y or moons[i].velocity.y != initial_state[i].velocity.y:
                is_y_cycle = False

            if moons[i].position.z != initial_state[i].position.z or moons[i].velocity.z != initial_state[i].velocity.z:
                is_z_cycle = False

            if not (is_x_cycle or is_y_cycle or is_z_cycle):
                break

        if cycle_x == -1 and is_x_cycle:
            cycle_x = steps
        if cycle_y == -1 and is_y_cycle:
            cycle_y = steps
        if cycle_z == -1 and is_z_cycle:
            cycle_z = steps

    # find the Least common multiplier of all three cycles
    # it will be the amount of steps after wich all three
    # dimensions are in initial state
    system_cycle = lcm(cycle_x, lcm(cycle_y, cycle_z))
    print(system_cycle)

def lcm(var_a, var_b):
    return (var_a * var_b) // gcd(var_a, var_b)

def gcd(var_a, var_b):

    while var_b != 0:
        temp = var_b
        var_b = var_a % var_b
        var_a = temp
    return var_a

def simulate_step(moons):
    """
    Simulate a single step of the system
    Every pair of moons is examined and their velocity and position updated
    """

    for i in range(len(moons)):
        for j in range(i, len(moons)):
            moon1 = moons[i]
            moon2 = moons[j]

            moon1_vel_change = Vector(0, 0, 0)
            moon2_vel_change = Vector(0, 0, 0)

            x_diff = moon1.position.x - moon2.position.x
            y_diff = moon1.position.y - moon2.position.y
            z_diff = moon1.position.z - moon2.position.z

            if x_diff < 0:
                moon1_vel_change.x = 1
                moon2_vel_change.x = -1
            elif x_diff > 0:
                moon1_vel_change.x = -1
                moon2_vel_change.x = 1

            if y_diff < 0:
                moon1_vel_change.y = 1
                moon2_vel_change.y = -1
            elif y_diff > 0:
                moon1_vel_change.y = -1
                moon2_vel_change.y = 1

            if z_diff < 0:
                moon1_vel_change.z = 1
                moon2_vel_change.z = -1
            elif z_diff > 0:
                moon1_vel_change.z = -1
                moon2_vel_change.z = 1

            moon1.update_velocity(moon1_vel_change)
            moon2.update_velocity(moon2_vel_change)

    for moon in moons:
        moon.apply_velocity()
        moon.update_energy()

main()
