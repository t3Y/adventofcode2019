#!/usr/bin/env python3
"""
Moon and Vector objects to make the task easier
"""

class Moon(object):
    """a moon has a position, velocity, and energy"""

    def __init__(self, x, y, z):

        self.position = Vector(x, y, z)
        self.velocity = Vector(0, 0, 0)
        self.potential_energy = 0
        self.kinetic_energy = 0
        self.total_energy = 0

    def __str__(self):
        return "pos:{}, vel:{}, pot:{}, kin:{}, tot:{}".format(
            self.position,
            self.velocity,
            self.potential_energy,
            self.kinetic_energy,
            self.total_energy)


    def apply_velocity(self):

        self.position.add(self.velocity)

    def update_velocity(self, diff):

        self.velocity.add(diff)

    def update_energy(self):

        self.potential_energy = abs(self.position.x) + abs(self.position.y) + abs(self.position.z)
        self.kinetic_energy = abs(self.velocity.x) + abs(self.velocity.y) + abs(self.velocity.z)
        self.total_energy = self.potential_energy * self.kinetic_energy


class Vector(object):

    def __init__(self, x, y, z):

        self.x = x
        self.y = y
        self.z = z

    def __str__(self):

        return "<{}, {}, {}>".format(self.x, self.y, self.z)

    def add(self, vector):

        self.x += vector.x
        self.y += vector.y
        self.z += vector.z
