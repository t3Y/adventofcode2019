#!/usr/bin/env python3

from body import Body

def main():
    file = open("./input", "r")
    orbits = [l.rstrip() for l in file]
    file.close()

    orbitalMap = {};

    for orbit in orbits:
        objects = orbit.split(")")
        centerName = objects[0]
        orbiterName = objects[1]

        if centerName in orbitalMap:
            center = orbitalMap[centerName]
        else:
            center = Body(centerName)

        
        if orbiterName in orbitalMap:
            orbiter = orbitalMap[orbiterName]
        else:
            orbiter = Body(orbiterName)

        orbiter.setCenter(center)
        orbitalMap[centerName] = center
        orbitalMap[orbiterName] = orbiter


    # for both objects SAN and YOU, follow the chain of orbits.
    # keep track of all objects on the way, find the closest object common to both chains
    hopsCount = 0

    you = orbitalMap["YOU"]
    chainContentYou = set()
    chainYOU = []

    center = you.getCenter()
    while center != None:
        chainContentYou.add(center.getName())
        chainYOU.append(center)
        center = center.getCenter()

    # count hops towards the closest common body
    santa = orbitalMap["SAN"]
    center = santa.getCenter()
    while center.getName() not in chainContentYou:
        center = center.getCenter()
        hopsCount += 1

    closestCommonBody = center.getName()

    # count hops in the YOU chain towards closest common body
    for body in chainYOU:
        if body.getName() == closestCommonBody:
            break
        else:
            hopsCount += 1

    print(hopsCount)


main()
