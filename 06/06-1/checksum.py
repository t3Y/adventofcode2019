#!/usr/bin/env python3

from body import Body

def main():
    file = open("./input", "r")
    orbits = [l.rstrip() for l in file]
    file.close()

    orbitalMap = {};

    for orbit in orbits:
        objects = orbit.split(")")
        centerName = objects[0]
        orbiterName = objects[1]

        if centerName in orbitalMap:
            center = orbitalMap[centerName]
        else:
            center = Body(centerName)

        
        if orbiterName in orbitalMap:
            orbiter = orbitalMap[orbiterName]
        else:
            orbiter = Body(orbiterName)

        orbiter.setCenter(center)
        orbitalMap[centerName] = center
        orbitalMap[orbiterName] = orbiter

    orbitCount = 0

    # for each object, follow the chain of orbits all the way to the COM.
    # this works assuming there are no orbit loops
    for body in orbitalMap:
        center = orbitalMap[body].getCenter()
        while center != None:
            orbitCount += 1
            center = center.getCenter()

    print(orbitCount)

main()
