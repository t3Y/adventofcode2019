#!/usr/bin/env python3

def main():
    input_str = "59782619540402316074783022180346847593683757122943307667976220344797950034514416918778776585040527955353805734321825495534399127207245390950629733658814914072657145711801385002282630494752854444244301169223921275844497892361271504096167480707096198155369207586705067956112600088460634830206233130995298022405587358756907593027694240400890003211841796487770173357003673931768403098808243977129249867076581200289745279553289300165042557391962340424462139799923966162395369050372874851854914571896058891964384077773019120993386024960845623120768409036628948085303152029722788889436708810209513982988162590896085150414396795104755977641352501522955134675"

    input_arr = []
    for char in input_str:
        input_arr.append(int(char))

    res = apply_fft(input_arr, 100)
    solution = ""
    for i in range(8):
        solution += str(res[i])

    print(solution)

def apply_fft(arr, phases):

    if phases == 0:
        return arr

    new_arr = []
    base_pattern = [0, 1, 0, -1]

    for index in range(len(arr)):
        pattern_index = 0
        repeats = 1

        total = 0
        for digit in arr:
            if repeats == index + 1:
                pattern_index = (pattern_index + 1) % 4
                repeats = 1
            else:
                repeats = repeats + 1
            total += base_pattern[pattern_index] * digit

        total = abs(total) % 10
        new_arr.append(total)

    return apply_fft(new_arr, phases-1)

main()
