#!/usr/bin/env python3

def main():
    """
    solution only works because offset it big enough
    all multipliers at position after offset are just 1
    """

    input_str = "59782619540402316074783022180346847593683757122943307667976220344797950034514416918778776585040527955353805734321825495534399127207245390950629733658814914072657145711801385002282630494752854444244301169223921275844497892361271504096167480707096198155369207586705067956112600088460634830206233130995298022405587358756907593027694240400890003211841796487770173357003673931768403098808243977129249867076581200289745279553289300165042557391962340424462139799923966162395369050372874851854914571896058891964384077773019120993386024960845623120768409036628948085303152029722788889436708810209513982988162590896085150414396795104755977641352501522955134675"


    offset = int(input_str[:7])

    # keep only integers after the offset as input, as values of previous ones have
    # no impact on final result
    negative_offset = len(input_str) * 10000 - (offset + 1)
    input_arr = []
    while len(input_arr) < negative_offset:
        for char in input_str:
            input_arr.append(int(char))

    res = apply_fft(input_arr, 100)
    solution = ""
    for i in res[(-negative_offset - 1) : -(negative_offset - 7)]:
        solution += str(i)

    print(solution)


def apply_fft(arr, phases):
    """
    All values after the offset are just the result of the sum
    of all values at index equal or higher
    this means we can keep the modulo 10 of the total, and
    gradually overwrite the array starting from the end
    """

    if phases == 0:
        return arr

    total = 0

    length = len(arr)
    for i in range(length):
        index = length - (i + 1)
        total = (total + arr[index]) % 10
        arr[index] = total

    return apply_fft(arr, phases-1)

main()
